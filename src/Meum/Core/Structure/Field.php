<?php

namespace Meum\Core\Structure;

use Meum\Core\Collection\NamedInterface;

class Field implements NamedInterface
{
    public const TYPE_VARCHAR = 'varchar';
    public const TYPE_TEXT = 'text';
    public const TYPE_INT = 'int';
    public const TYPE_FLOAT = 'float';
    public const TYPE_BOOL = 'bool';

    public const ATTR_UNSIGNED = 'unsigned';
    public const ATTR_NULLABLE = 'nullable';
    public const ATTR_AUTO_INCREMENT = 'auto_increment';
    public const ATTR_DEFAULT = 'default';

    public static function primaryKey(string $name): self
    {
        return new static($name, self::TYPE_VARCHAR, null, [
            self::ATTR_UNSIGNED => true,
            self::ATTR_NULLABLE => false,
            self::ATTR_AUTO_INCREMENT => true,
        ]);
    }

    public static function varchar(string $name, ?int $size = null): self
    {
        return new static($name, self::TYPE_VARCHAR, $size);
    }

    public static function text(string $name): self
    {
        return new static($name, self::TYPE_TEXT);
    }

    public static function bool(string $name, ?bool $default = null): self
    {
        return new static($name, self::TYPE_BOOL, null, [

        ]);
    }

    private string $name;
    private string $type;
    private ?int $size;

    private array $attributes = [
        self::ATTR_UNSIGNED => false,
        self::ATTR_NULLABLE => true,
        self::ATTR_AUTO_INCREMENT => false,
        self::ATTR_DEFAULT => null,
    ];

    private function __construct(string $name, string $type, ?int $size = null, array $attributes = [])
    {
        $this->name = $name;
        $this->type = $type;
        $this->size = $size;

        foreach ($attributes as $key => $value) {
            $this->attributes[$key] = $value;
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function isUnsigned(): bool
    {
        return $this->attributes[self::ATTR_UNSIGNED];
    }

    public function isNullable(): bool
    {
        return $this->attributes[self::ATTR_NULLABLE];
    }

    public function isAutoIncrement(): bool
    {
        return $this->attributes[self::ATTR_AUTO_INCREMENT];
    }

    public function getDefault()
    {
        return $this->attributes[self::ATTR_DEFAULT];
    }
}