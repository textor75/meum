<?php

namespace Meum\Core\Collection;

interface NamedInterface
{
    public function getName(): string;
}