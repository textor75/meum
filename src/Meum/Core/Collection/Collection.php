<?php

namespace Meum\Core\Collection;

class Collection
{
    protected ?string $itemClass;
    protected array $items;

    public function __construct(?string $itemClass = null, array $items = [])
    {
        $this->items = $items;
        $this->itemClass = $itemClass;
    }

    public function addItem($item): int
    {
        $this->checkItem($item);
        $this->items[] = $item;

        return $this->count();
    }

    public function count(): int
    {
        return count($this->items);
    }

    protected function checkItem($item): void
    {
        if ($this->itemClass === null) {
            return;
        }

        if (!is_object($item)) {
            throw new \DomainException('Item is not object');
        }

        if (!($item instanceof $this->itemClass)) {
            throw new \DomainException('Item must be instance of ' . $this->itemClass);
        }
    }
}
