<?php

namespace Meum\Core\Collection;

class NamedCollection extends Collection
{
    public function __construct(?string $itemClass = null, array $items = [])
    {
        parent::__construct($itemClass, $items);
        $this->checkItemClass($itemClass);
    }

    /**
     * @param NamedInterface $item
     * @return int
     */
    public function addItem($item): int
    {
        $this->checkItem($item);
        $name = $item->getName();
        $this->checkName($name);

        $this->items[$name] = $item;

        return $this->count();
    }

    protected function checkItemClass(?string $itemClass): void
    {
        if (!($itemClass instanceof NamedInterface)) {
            throw new \DomainException('ItemClass must be instance of NamedInterface');
        }
    }

    protected function checkName(string $name): void
    {
        if (isset($this->items[$name])) {
            throw new \DomainException('Collection just contains item with name ' . $name);
        }
    }
}
