<?php
namespace Meum\Core\App;

use Meum\Core\Request\RequestInterface;
use Meum\Core\Config\ConfigInterface;

class App implements AppInterface
{
	private ConfigInterface $config;

	public static function configure(ConfigInterface $config): self
	{
		return new static($config);
	}

	public function __construct(ConfigInterface $config)
	{
		$this->config = $config;
	}

	public function run(RequestInterface $request): void
	{
        if ($request->isCli()) {

        }
	}
}
