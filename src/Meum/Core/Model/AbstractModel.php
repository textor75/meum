<?php

namespace Meum\Core\Model;

use Meum\Core\Collection\NamedCollection;
use Meum\Core\Structure\Field;

abstract class AbstractModel
{
    private static array $meStructure = [];

    protected static $tableName;

    public static function getStructure(): array
    {
        $className = get_called_class();
        if (!isset($meStructure[$className])) {
            $meStructure[$className] = static::getFields();
        }

        return $meStructure[$className];
    }

    public static function getTableName(): string
    {
        if (!isset(static::$tableName)) {
            throw new \DomainException('Table name is undefined');
        }

        return static::$tableName;
    }

    protected static function getFields(): NamedCollection
    {
        return static::getFieldsCollection();
    }

    protected static function getFieldsCollection(): NamedCollection
    {
        return new NamedCollection(Field::class);
    }
}
