<?php

namespace Meum\Core\Command;

use Meum\Core\App\AppInterface;

abstract class AbstractCommand
{
    public function __construct(AppInterface $app)
    {

    }

    abstract public function run(): void;
}