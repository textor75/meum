<?php
namespace Meum\Core\Request;

interface RequestInterface
{
    public function isCli(): bool;

    public function getMethod(): ?string;

    public function isGet(): bool;

    public function isPost(): bool;

    public function getData(string $name, mixed $default = null): mixed;

    public function getArgs(int $index, mixed $default = null): string;
}
