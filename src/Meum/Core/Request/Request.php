<?php
namespace Meum\Core\Request;

class Request implements RequestInterface
{
    public const ATTRIBUTES = 'attributes';
    public const DATA = 'data';
    public const ARGS = 'args';
    public const HEADERS = 'headers';
    public const SESSIONS = 'sessions';
    public const COOKIES = 'cookies';

    public const ATTR_CLI = 'cli';
    public const ATTR_METHOD = 'method';

    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';

    protected array $attributes = [];
    protected array $data = [];
    protected array $args = [];
    protected array $headers = [];
    protected array $sessions = [];
    protected array $cookies = [];

	public function __construct(array $params = [])
    {
        $this->attributes = array_merge($this->prepareAttributes(), $params[self::ATTRIBUTES] ?? []);
        $this->data = array_merge($this->prepareData(), $params[self::DATA] ?? []);
        $this->args = array_merge($this->prepareARGS(), $params[self::ARGS] ?? []);
        $this->headers = array_merge($this->prepareHeaders(), $params[self::HEADERS] ?? []);
        $this->sessions = array_merge($this->prepareSessions(), $params[self::SESSIONS] ?? []);
        $this->cookies = array_merge($this->prepareCookies(), $params[self::COOKIES] ?? []);
	}

    public function isCli(): bool
    {
        return $this->attributes[self::ATTR_CLI];
    }

    public function getMethod(): ?string
    {
        return $this->attributes[self::ATTR_METHOD];
    }

    public function isGet(): bool
    {
        return $this->getMethod() === self::METHOD_GET;
    }

    public function isPost(): bool
    {
        return $this->getMethod() === self::METHOD_POST;
    }

    public function getData(string $name, mixed $default = null): mixed
    {
        return $this->data[$name] ?? $default;
    }

    public function getArgs(int $index, mixed $default = null): string
    {
        return $this->args[$index] ?? $default;
    }

    protected function prepareAttributes(): array
    {
        return [
            self::ATTR_CLI => $this->checkIsCli(),
            self::ATTR_METHOD => isset($_SERVER['REQUEST_METHOD']) ? strtoupper($_SERVER['REQUEST_METHOD']) : null,
        ];
    }

    protected function prepareData(): array
    {
        if ($this->isCli()) {
            return [];
        }
        if ($this->isGet()) {
            return $_GET;
        }
        if ($this->isPost()) {
            return $_POST;
        }
        return [];
    }

    protected function prepareArgs(): array
    {
        return $this->isCli() ? $_SERVER['argv'] : [];
    }

    protected function prepareHeaders(): array
    {
        return function_exists('getallheaders') ? getallheaders() : [];
    }

    protected function prepareSessions(): array
    {
        session_start();
        return $_SESSION;
    }

    protected function prepareCookies(): array
    {
        return $_COOKIE;
    }

    protected function checkIsCli(): bool
    {
        return php_sapi_name() === 'cli';
    }
}