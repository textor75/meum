<?php

namespace Meum\Core\Config;

class Config implements ConfigInterface
{
	private array $config;

	public function __construct(string $path)
	{
		$this->config = require_once $path;
	}
}