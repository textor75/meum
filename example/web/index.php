<?php

use Meum\Core\App\App;
use Meum\Core\Request\Request;
use Meum\Core\Config\Config;

include_once __DIR__ . '/../../vendor/autoload.php';

$config = new Config(__DIR__ . '/../config/conf.php');
$request = new Request();

App::configure($config)->run($request);
