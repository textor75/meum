<?php

return [
    'GET' => [
        '' => 'index',
        'products' => 'products/products',
        'products/{id:int}' => 'products/product',
        '{page:string}' => 'page',
    ],
];
