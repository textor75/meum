<?php

namespace App\Model;

use Meum\Core\Collection\NamedCollection;
use Meum\Core\Model\AbstractModel;
use Meum\Core\Structure\Field;

/**
 * @property integer id
 * @property string name
 * @property string title
 * @property string $path
 * @property bool $active
 * @property string $content
 */
class Page extends AbstractModel
{
    protected static $tableName = 'page';

    protected static function getFields(): NamedCollection
    {
        $fields = static::getFieldsCollection();
        $fields->addItem(Field::primaryKey('id'));
        $fields->addItem(Field::varchar('name', 128));
        $fields->addItem(Field::varchar('title', 255));
        $fields->addItem(Field::varchar('path', 255));
        $fields->addItem(Field::bool('active', true));
        $fields->addItem(Field::text('content'));

        return $fields;
    }
}
